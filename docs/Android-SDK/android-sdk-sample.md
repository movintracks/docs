#Running the Sample in Android Studio

Open Android Studio and click Import Non-Android Studio project:

![Plist file](../img/android_studio_import.png) 

Select the Sample Project:

![Plist file](../img/android_studio_select.png) 

Add your server, API Key and API Secret in the Credentials file:

![Plist file](../img/android_studio_credentials.png) 

Then you can run the project and enjoy the Movintracks SDK.  

