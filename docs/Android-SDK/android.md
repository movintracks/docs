## Folders
These are the folders you will find in this archive:

1. **Sample**: an [example](android-sdk-sample.md) of how to work with our library. 
2. **SDK-Movintracks**:
  * **dependencies**: Contains androidBeaconLibrary.aar and sdk-facebook.aar. These libraries should be added through the 'add new module' option in Android Studio.
    *  **androidBeaconLibrary.aar**: an open source library used to manage Beacons by Radius Networks.
  * **docs**: a doxygen-generated documentation of the main classes and instances of our library.
  * **library**: Contains sdk-movintracks.aar file that you should import like the previous dependencies. 

## Installation

Follow these steps to integrate the Movintracks SDK within an existing project:

1. Import androidBeaconLibrary.aar and sdk-movintracks.aar. File > New Module > More Modules > Import .JAR or .AAR Package. 

2. You should add the following URIs in the Facebook Developer's console in "Valid OAuth Redirect URIs": `http://www.facebook.com` and the full URI of your Movintracks installation (typically `https://api.movintracks.io/`).
 
3. Add dependencies to gradle file:
  * ```'com.google.android.gms:play-services-gcm:7.5.0'```
  * ```'com.google.android.gms:play-services-location:7.5.0'```
  * ```'com.google.code.gson:gson:2.3'```
  * ```'com.octo.android.robospice:robospice-google-http-client:1.4.14'```
  * ```'com.google.http-client:google-http-client-gson:1.19.0'```
  * ```'org.twitter4j:twitter4j-async:4.0.1'```
  * ```'org.twitter4j:twitter4j-core:4.0.1'```
  * ```'org.twitter4j:twitter4j-media-support:4.0.1'```
  * ```'com.facebook.android:facebook-android-sdk:4.1.1'```
    
    

## Eclipse Installation Notes
1. To import the aar files, you should change the aar extension to zip, and decompress the file and move classes.jar file from the root decompressed folder to the libs folder (create if it doesn't exist) and change the name of the file classes.jar to $library.jar. Now, you can import the zip contents as Android code into the Eclipse workspace.
2. Enable mergermanifest tool adding the line ```manifestmerger.enabled=true``` to project.properties on the application project.

