##1.11
  * Upgraded facebook SDK to 4.4.0.
  * Deprecates customerID.

##1.10
  * New Action: Oracle sales cloud create activity.

##1.9
  * Bug fixes

##1.8.2
  * Bug fixes

##1.8.1
  * Adds launch context information to the "custom app callback" action
  * Bug fixes
  

##1.8
  * Upgraded facebook SDK to 4.1.1 
  * New location stats in zone visits.
  * Updated google play services to 7.5
  * Updated android beacon library to 2.1.4
  * Bug fixes

##1.7.1
  * Solves a bug when storing the device information. 
 
##1.7
  * Stop using Guava
  * Bug fixes

##1.6
  * New on visit zone trigger

##1.5
  * Expose device id to the client application
  * Updated dependencies
  * Bug fixes

##1.4.1
  *  Can receive UUID updates

##1.4
  *  Migrated to Movintracks API v2.0
  *  Bug fixes

##1.3 
  *  Migrated to Android Beacon Library 2.0
  *  Improved battery usage
  *  Upgraded to the new facebook API
  *  Bug fixes
  
##1.2.4
  * Changed deprecated ```runproguard``` call on Sample project
  * Removed deprecated Facebook action permission
  * Improved README
  * Improved Sample code
  * BluetoothDisabled notification changed
  * SDK Packaged as AAR

##1.2.3
  * Bug fixes
  * Improved Geofence detection
  * Improved Bluetooth management

##1.2.2
  * Improved documentation

##1.2.1
  * Added Google Cloud message managing methods to force data update from the server
  * Changed the geographical save data to the new API specs
  * Enforced backwards-compatibility (until Gingerbread)
  * Simplified the onActivity result management of the library
  
##1.2
  * Added parameters on Movintracks constructor
  * Added two new methods on iMovintracks interface: customCallBackAction and beaconsAvailable


