#Integration

##Create a new Movintracks class instance

Create a new Movintracks class instance inside the main class, extending Application class and implementing IMovintracks. If you don't have one, create it and register it on AndroidManifest.xml. These are the Movintracks Constructor parameters:

* **app**: the Application class instance. 
* **rootServer**: The URI of your Movintracks installation. Typically `https://api.movintracks.io/`.
* **apiKey**, **apiSecret**: the pair of strings provided to you for auth purposes. You can get yours from the dashboard, at Profile > API/SDK.
* **googleProjectNumber**, **googleAPIKey**: two strings used for auth purposes in Google Cloud Messaging (GCM). You can get yours from your Google Cloud Message (GCM) console. 
* **bExecuteIntent**: boolean that should be set to **true** if the library needs to launch an external browser when delivering an action to the user, or to **false** if your own app handles these actions. 

##Implement Movintracks methods

* **void launchWebView(java.lang.String s)**: function used by Movintracks to launch a web view inside the app according with your choice in the parameter `bExecuteIntent`.
* **void customCallBackAction(io.movintracks.library.interfaces.ActionInterface actionInterface, java.lang.String name, java.util.HashMap<java.lang.String,java.lang.String> args)**:  function called by Movintracks SDK to run the action "Custom app callback". Use **actionInterface** to call the `.finish()` method when the action is done. **name** is the name of the callback set in the dashboard. **args** are the arguments that are set in the dashboard. From version v1.8.1, the next arguments are added by default to the callback:
    * `mt_campaign_id`: The id of the campaign. 
    * `mt_zone_id`: The id of the zone.
    * `mt_point_id`: The id of the point. 0 if it is an onVisitZone campaign.
    * `mt_beacon_id`: The id of the beacon. 0 if it is an onVisitZone campaign.	  

##onActivityResult

As in the previous point, on all Activities onActivityResult method, call the function Movintracks.onActivityResult(int requestCode, int resultCode, Intent data)

##Manage GCM

* GCM-enable your app using the Movintracks' SDK constant ```io.movintracks.library.Constants.GCM.SENDER_ID```. You will get an string from this call, now call ```io.movintracks.library.Movintracks.setGoogleCloudRegistrationId()``` using the string you just got from the previous call as the parameter. 
* When the application receives a new Google Cloud Message via push, you need to call the method ```io.movintracks.library.Movintracks.receivedGoogleCloudMessage()``` with one parameter: the bundled data received in the Intent instance (from the push). See ```ìo.movintracks.demo.remoteNotifications.GcmIntentService``` from MovintracksDemo (included in the archive) sample code for further reference.

See http://developer.android.com/google/gcm/client.html for futher details.

##Modify Android Manifest

* Add the next Intent-filter inside the MainActivity tag:

```
<intent-filter>
    <action android:name="android.intent.action.VIEW" />
    <category android:name="android.intent.category.DEFAULT" />
    <category android:name="android.intent.category.BROWSABLE" />
    <data 
        android:scheme="oauth" 
        android:host="t4jsample"/>
</intent-filter>
```
##Facebook

In order to use the social actions with Facebook, you need to follow the next steps:

* Initialize Facebook SDK before you can use it. Add a call to FacebookSdk.sdkInitialize from onCreate in Activity or Application:

```
@Override
 public void onCreate(Bundle savedInstanceState) {
     super.onCreate(savedInstanceState);
     FacebookSdk.sdkInitialize(getApplicationContext());
}
```

* Add Facebook App ID:

```
<application android:label="@string/app_name" ...>
    ...
    <meta-data android:name="com.facebook.sdk.ApplicationId" android:value="@string/facebook_app_id"/>
    ...
</application>
```

* Add the FacebookActivity to the manifest:

```
<activity android:name="com.facebook.FacebookActivity"
          android:configChanges=
                 "keyboard|keyboardHidden|screenLayout|screenSize|orientation"
          android:theme="@android:style/Theme.Translucent.NoTitleBar"
          android:label="@string/app_name" />
```

###Read Permissions

Movintracks requires **user_likes** permission. If you don't want to manage the Facebook login add this code in your activity:

```
@Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        callbackManager = CallbackManager.Factory.create();
        movintracksInstance = Movintracks.getInstance();
        movintracksInstance.loginWithReadPermissionsInFacebook(callbackManager);
    }   
```

If you want to manage the Facebook login, add a call to updateMovintracksSocialProfiles() in your onSuccess callback:

```
// Callback registration
    loginButton.registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
        @Override
        public void onSuccess(LoginResult loginResult) {
            movintracksInstance.updateMovintracksSocialProfiles();
            // App code
        }

        @Override
        public void onCancel() {
            // App code
        }

        @Override
        public void onError(FacebookException exception) {
            // App code
        }
    });    
```

###Publish Permissions

Movintracks requires **publish_actions** permission. If you don't want to manage the task of asking for publish permissions, you can call setAuthorizedToRequestPublishPermissions(true) and setCallbackManager(callbackManager) and the SDK will ask for the permissions before publishing to Facebook.

```
@Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        movintracksInstance = Movintracks.getInstance();
        movintracksInstance.setCallbackManager(callbackManager);
        movintracksInstance.setAuthorizedToRequestPublishPermissions(true);
}
```

If you want to manage the publish permissions, AuthorizedToRequestPublishPermissions is set to false by default.

##Twitter

Create the file **assets/oauth_consumer.properties** with the application keys for Twitter. See the keys required on the following example:

```
#twitter
twitter.com.consumer_key = <# twitter consumer key #>
twitter.com.consumer_secret = <# twitter consumer secret #>
```
      
##Android 2.3

To run the SDK on Android 2.3 (and other legacy versions aswell) is mandatory to shrink the bytecode with proguard. An example of a running proguard configuration is:

```
-dontwarn twitter4j.**
-dontwarn com.google.common.**
-dontwarn com.octo.android.**

-dontobfuscate

-keep class twitter4j.** {
  *;
}
-keep class com.facebook.** {
 *;
}

-keep public class * extends android.app.Activity
-keep public class * extends android.app.Application
-keep public class * extends android.app.Service
-keep public class * extends android.content.BroadcastReceiver
-keep public class * extends android.content.ContentProvider
-keep public class * extends android.app.backup.BackupAgentHelper
-keep public class * extends android.preference.Preference
-keep public class com.android.vending.licensing.ILicensingService
```
  
##Other Options   
Create a new class that extends Movintracks and overrides these methods to customize the default behavior of the SDK in these cases:

  * **void geofenceNotAvailable()**: Function called by Movintracks SDK to notify the app that a geofence is not available.
  * **void bluetoothDisabled(String message)**: Function to notify the app that bluetooth is disabled, so the SDK won't be able to listen beacons. Parameter ```message``` is the message configured in Zones on Movintracks Dashboard. 
  * **void bluetoothLENotAvailable()**: Function to notify the app that the device don't have low energy bluetooth capabilities.

##String Customization

Here is the list of strings that you can customize or translate (Format: ```key```: Value) :
  
  * ```mt_pick_image_camera_button```: Camera
  * ```mt_pick_image_gallery_button```: Gallery
  * ```mt_pick_image_select_title```: Select
  * ```mt_social_request_permission_tittle```: Warning
  * ```mt_social_request_publish_message```: Publish a message on {param} account?
  * ```mt_social_request_access_message```: Did you authorize to access {param} account?
  * ```mt_social_accept_button```: Accept
  * ```mt_social_retry_button```: Retry
  * ```mt_social_cancel_button```: Cancel
  * ```mt_social_pick_image_title```: Select image source to post
  * ```mt_social_pick_image_message```: Please select an image to post on your {param} account.
  * ```mt_social_post_message_title```: Published correctly
  * ```mt_social_post_message_text```: The post was correctly published on %@
  * ```mt_social_post_message_ok```: OK
  * ```mt_social_post_error_message_title```: Error publishing
  * ```mt_social_post_error_message_text```: There was an error publishing on %@
  * ```mt_social_request_not_publish_button```: Not publish
  * ```mt_location_services_required_title```: Location services disabled
  * ```mt_location_services_required_message```: This application needs access to your location. Please turn on location access.
  * ```mt_location_services_required_ok```: Settings
  * ```mt_location_services_required_cancel```: Ignore
  * ```mt_enable_bluetooth_title```: Bluetooth required
  * ```mt_enable_bluetooth_text```: Please enable bluetooth to improve the experience.
  * ```mt_dismiss_bluetooth_notification```: Don't show again


