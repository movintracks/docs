Installation
------------
Add the plugin to a project.
With Phonegap:

    $ phonegap local plugin add https://github.com/movintracks/cordova-plugin --variable FACEBOOK_ID=<your fb app id>
    
With Cordova:

    $ cordova plugin add https://github.com/movintracks/cordova-plugin --variable FACEBOOK_ID=<your fb app id>

`FACEBOOK_ID` must be defined for the plugin to be installed.