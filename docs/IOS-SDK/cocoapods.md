# Cocoapods

###Installing Cocoapods
To install cocoapods you should open a terminal (```/Applications/Utilities/Terminal.app```), and run the following command line:

```BASH
sudo gem install cocoapods
```
If everything is OK, then you can run the following line with the showed output:

```BASH
pod --help
```
Output:

```
Usage:

    $ pod COMMAND

      CocoaPods, the Objective-C library package manager.

Commands:

    + init                Generate a Podfile for the current directory.
    + install             Install project dependencies to Podfile.lock versions
    + ipc                 Inter-process communication
    + lib                 Develop pods
    + list                List pods
    + outdated            Show outdated project dependencies
    + plugins             Show available CocoaPods plugins
    + repo                Manage spec-repositories
    + search              Searches for pods
    + setup               Setup the CocoaPods environment
    + spec                Manage pod specs
    + trunk               Interact with the CocoaPods API (e.g. publishing new specs)
    + try                 Try a Pod!
    + update              Update outdated project dependencies and create new
                          Podfile.lock

Options:

    --silent              Show nothing
    --completion-script   Print the auto-completion script
    --version             Show the version of the tool
    --verbose             Show more debugging information
    --no-ansi             Show output without ANSI codes
    --help                Show help banner of specified command
```

See http://cocoapods.org for futher reference.