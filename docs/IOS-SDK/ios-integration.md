# Integration

Here you are some guidelines on how to get Movintracks' SDK running within your project. Additionally you can take a look at our [sample project](ios-sdk-sample.md) . 

##In your application's **Info.plist**
    
Set the property **FacebookAppID** according to your Facebook app ID. See [here](https://developers.facebook.com/docs/ios/getting-started/) for futher reference. Note that at Facebook Developer's console you should add **http://www.facebook.com** and the full URI of your Movintracks installation (typically **https://api.movintracks.io/**) to "Valid OAuth Redirect URIs". 

Set the property **NSLocationAlwaysUsageDescription** with a description text explaining why CoreLocation permissions are required. You can localize the text at InfoPlist.strings with key **NSLocationAlwaysUsageDescription**.
    
Set the **UIBackgroundModes**. You have to add the next values: "fecht and remote-notification". More information [here](https://developer.apple.com/library/ios/documentation/General/Reference/InfoPlistKeyReference/Articles/iPhoneOSKeys.html#//apple_ref/doc/uid/TP40009252-SW22).

Your plist should look like this:
    
![Plist file](../img/ios-plist.png) 

##In your **AppDelegate.h** 

Start by importing the library: 

```
import "MTMovintracks.h"
```   
    
##In your **AppDelegate.m**

In the method **didFinishLaunchingWithOptions:**, add the following: 

```
[[MTMovintracks alloc] initWithRootServer:<# Base Url #> 
                       ApiKey:<# Api User Key #> 
                       andApiSecret:<# Api User Secret #> 
                       withLaunchingOptions: <# launchOptions #>];
```

Tags are:

-   Base Url: the URL of your Movintracks installation. Typically `https://api.movintracks.io/`.
-   Api User Key: application identifier that can be found at `Profile > API/SDK` of the Movintracks Dashboard.
-   Api User Secret: secret key used to sign the comunication with the server. You can also find it at `Profile > API/SDK`.
-   Launch options: The dictionary with launch options, used to get info about the notification if the app has been launched because the user has clicked on it.

In the method **applicationDidBecomeActive:** add the following: 
```
[<# movinTracksInstance #> applicationDidBecomeActive];
```
In the method **didReceiveLocalNotification:** add the following: 
```
[<# movinTracksInstance #> applicationDidReceiveLocalNotification:notification];
```
##Register silent notifications and send the push token to the SDK.

In the method **didRegisterForRemoteNotificationsWithDeviceToken:** add the following:
```
[<# movinTracksInstance #> setPushToken:deviceToken];
```

In the method **didFailToRegisterForRemoteNotificationsWithError** add the following:
```
[<# movinTracksInstance #> setPushToken:nil];
```

In the method **didReceiveRemoteNotification:(NSDictionary *)userInfo fetchCompletionHandler:** add the following:
```
BOOL r = [<# movinTracksInstance #> applicationDidReceiveRemoteNotification: userInfo];
```

In the method **didFinishLaunchingWithOptions:** add the following code to enable both push notification and local notifications:
``` obj-c
// Check if we are using ios8
if ([application respondsToSelector:@selector(registerForRemoteNotifications)]){
  // Register remote notifications in iOS8
  [application registerForRemoteNotifications];
  // Ask user enable notifications in notification manager
  [application registerUserNotificationSettings:[UIUserNotificationSettings settingsForTypes:UIUserNotificationTypeSound|UIUserNotificationTypeAlert categories: nil]];
    } else {
    // Register to obtain push silent notifications
    [application registerForRemoteNotificationTypes:UIRemoteNotificationTypeNewsstandContentAvailability];
}
```

##Movintracks notifications

Movintracks SDK can emit the following notifications: 

- **kShowModalViewNotification**: It is used to let the app know what views should be shown. In the object of the notification, there is the view controller that should be shown. This is a sample code:

```obj-c
- (void)viewDidLoad
{
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(openModalView:) name:kShowModalViewNotification object:nil];
}
- (void) openModalView: (NSNotification*) notification{
    UIViewController* view=notification.object;
    [self presentViewController: view animated:YES completion:nil];
}
```

- **kNumberBeaconsNotification**: To debug the application and work properly with beacons, this notification contains the number of visible beacons. 

- **kListBeaconsNotification**: This notification contains the list of visible beacons, in the object property.

##Movintracks custom app callback

To run the action "Custom app callback", you need to register the callback:

```
[[MTMovintracks getInstance] registerCallback:^(NSDictionary *arguments, MTOnCompletionHandler handler) {
      //Your code here 
		NSLog(@"Custom App Callback with params %@", arguments.description);
		//Finish the action
		handler();
	} withName:@"MyCustomAppCallback"];
```
From version v1.8.1, the next arguments are added by default to the callback:

* `mt_campaign_id`: The id of the campaign. 
* `mt_zone_id`: The id of the zone. May be null in rare cases.  
* `mt_point_id`: The id of the point. Null if it is an onVisitZone campaign.
* `mt_beacon_id`: The id of the beacon. Null if it is an onVisitZone campaign or in rare cases.

