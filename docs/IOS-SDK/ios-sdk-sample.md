SDK-iOS
=======

## MovintracksDemo usage (Sample folder)

**Do not start by opening the project with MovintracksDemo.xcodeproj**. 

This project requires cocoapods utility installed, [here you have a some more information about Cocoapods.](cocoapods.md)

###Running Sample project

Go to the sample project folder using a command-line terminal and running ```pod install```. Then open `MovintracksDemo.xcworkspace` with xcode file, change parameters inside MovintracksDemo/MTConfig.h at your convenience with apiKey and apiSecret, and delete `#pragma error` line. After that, you can build and run the project on any device. 
