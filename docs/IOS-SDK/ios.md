# SDK Installation

To install Movintracks' SDK you have two options: use Cocoapods or manual instalation of the files. We recommend to use CocoaPods.

## Installation using Cocoapods.

[CocoaPods](cocoapods.md) is a dependency manager for Objective-C, that automates and simplifies the process of using 3rd-party libraries. 

Copy the SDK-Movintracks.podspec file from our SDK to your application project folder. Go to the project root folder using Terminal and run the following command to create the sample Podfile file configuration:

```
pod init
```

Edit the Podfile with any text editor, uncomment the line `platform :ios, '7.0'` to specify the minimum iOS deployment version to 7.0 and add the following line at the end to link on all targets:

```
pod 'SDK-Movintracks', :podspec => './SDK-Movintracks.podspec'
```

The file should look like this:

```
# Uncomment this line to define a global platform for your project
platform :ios, '7.0'

source 'https://github.com/CocoaPods/Specs.git'

target 'sample' do

end

target 'sampleTests' do

end

pod 'SDK-Movintracks', :podspec => './SDK-Movintracks.podspec'
```

Once you've done this, you can run ```pod install``` on the terminal and it should output the following information:

```
Analyzing dependencies
Fetching podspec for `SDK-Movintracks` from `./SDK-Movintracks.podspec`
Downloading dependencies
Installing AFNetworking (2.5.3)
Installing MWFeedParser (1.0.1)
Installing NSData+MD5Digest (1.0.0)
Installing SDK-Movintracks (1.5)
Generating Pods project
Integrating client project

[!] From now on use `sample.xcworkspace`.
```


Now you can open the file .xcworkspace to made the integration of Movintracks SDK

### Update SDK-Movintracks library

To update your project with a new version of Movintracks from a former one replace the SDK-Movintracks.podspec file with the new one provided in the new library. Then, run the following command line on the Terminal:

```BASH 
pod update
```

## Manual installation
Here, you can see how to install manually the SDK without Cocoapods. If you have an older version of the SDK Installed you only need to do points 1 and 2 replacing the old files.

1. Import `MTMovintracks.h` and `MTDefines.h` file headers to your project. They can be found at `sdk-ios/include/Movintracks`

    -   If your project needs to do direct calls to the same libraries that SDK-Movintracks uses, import their 
        headers files included in `sdk-ios/include/ExternalLibs` to your project. 

2.  Import the two static libraries found at `sdk-ios/lib`

3.  Add the `-all_load` flag to your project:

    `File project > Application > Build settings > Other linker flags` 

4. If you are using AFNetworking in your project, you will get a duplicate symbol error when you try to build. To avoid this problem, remove the .m files of AFNetworking.