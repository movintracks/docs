##1.11
- Deprecates customerID.
- Bug fixes.

##1.10
- New Action: Oracle sales cloud create activity.
- Bug fixes.

##1.9
- Solve warnings
- Remove unnecesary files

##1.8.2
- Bug fixes

##1.8.1
- Add launch context information to the "custom app callback" action

##1.8
- New location stats in zone visits
- Bug fixes

##1.7
- Improve behaviour of the SDK with overlapped zones
- Bug fixes

##1.6
- New on visit zone trigger

##1.5
- Expose device id to the client application

##1.4.2
- Remove Reachability dependency to avoid App rejection

##1.4.1
- Can receive UUID updates
- Bug fixes

##1.4
- Migrated to Movintracks API v2.0
- Bug fixes

##1.3
- Improved battery usage
- Improved behaviour in the background
- Improved management of the monitored zones
- Bug fixes

##1.2.4
- Removed deprecated Facebook action permission
- Improved README
- Improved sample code
- BluetoothDisabled notification changed

##1.2.3
- Bug fixes
- Improved bluetooth management when switching from on to off
- New feature: list what iBeacons are visible

##1.2.2
- Improved background campaign management
- Improved the WebViewController to manage correctly device orientation changes
- iOS8 compatibility fix

##1.2.1
- Changed the geographical saved data to the new API specs
- Enforced backwards-compatibility (iOS 6)

##1.2
- Added a new option to integrate SDK using cocoapods
- Full SDK version is now only compatible with Cocoapods
- Added push notification features

##1.1
-   Changed the name MTMovinTracks to MTMovintracks


